<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::Resource( '/products', 'productController' );
Route::apiResource( '/products', 'productController' ); // removes create and edit routes
Route::group( [ 'prefix' => 'products' ], function(){
	Route::apiResource( '/{product}/reviews', 'reviewController' );
});
